# CarCar

Team:

* Jeongwoo Heo - Services
* Person 2 - Which microservice?

## Setup

1) Clone the repository to your device using the command: git clone https://gitlab.com/jeongwoo9824/project-beta.git
2) Navigate to the \project-beta directory 
3) Run the command: docker volume create beta-data
4) Run the command: docker-compose build 
5) Run the command: docker-compose up
6) On your preferred web browser, type into the web search bar: http://localhost:3000
7) Upon pressing enter with the above address, you will be navigated to the project beta website

## Design

![Alt text](./image.png)

## Service microservice

The Inventory microservice (Run on Port 8100) contains the three models, Manufacturer, Automobile, and VehicleModel. Inventory is 
tasked with storing a list of cars, with their properties specified with the three models. Automobile is the value object that 
retrieves data from the inventory using the poller and sends it to the Services microservice (Run on Port 8080). AutomobileVO contains 
the VIN and the sold status, Technician contains first_name, last_name, and employee_id of the technician in charge of the services, 
and Appointment contains date_time, reason, status, VIN, customer, and technician, which pertains to information about the service. 
The website allows you to freely enter in data into a form regarding the car and the service for it. The forms you are able to enter 
data into are Manufacturers, Vehicle Models, Automobiles, Technicians and Service Appointments, all of which will output the data onto
a list that can also be accessed on the website as well. 

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

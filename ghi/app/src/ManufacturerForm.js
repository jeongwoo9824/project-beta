import React, {useState, useEffect} from 'react';

function ManufacturerForm() {
    const [name, setName] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        const manufactureUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(manufactureUrl, fetchConfig);
        if( response.ok){
            setName('')
        }
    }

    const handleNameChange = (event) => {
        setName(event.target.value)
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-Manufacturer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Manufacturer Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Manufacturer Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default ManufacturerForm

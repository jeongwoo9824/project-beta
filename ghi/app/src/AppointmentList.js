import React, { useState, useEffect } from 'react';

function AppointmentList(){
    const [appointments, setAppointments] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }

    useEffect(()=>{
        getData()
    }, [])
    
    const cancelAppointment = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`,
        {
        method: 'PUT',
        body: JSON.stringify({status: 'canceled'}),
        headers: {
            'Content-Type': 'application/json'
            }
        })
        if(response.ok){
            getData()
        }
    }

    const finishAppointment = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`,
        {
        method: 'PUT',
        body: JSON.stringify({status: 'finished'}),
        headers: {
            'Content-Type': 'application/json'
            }
        })
        if(response.ok){
            getData()
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date and Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
    
            <tbody>
                {appointments.map((appointment) => {
                return (
                    <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.vip}</td>
                    <td>{appointment.customer}</td>
                    <td>{appointment.date_time}</td>
                    <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                    <td>{appointment.reason}</td>
                    <td>
                        <button onClick={(e) => cancelAppointment(appointment.id)}>Cancel</button>
                    </td>
                    <td>
                        <button onClick={(e) => finishAppointment(appointment.id)}>Finish</button>
                    </td>
                    </tr>
                );
                })}
            </tbody>
        </table>
    );
}
    
    export default AppointmentList;
    

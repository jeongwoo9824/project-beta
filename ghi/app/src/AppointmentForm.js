import {useEffect, useState} from 'react'


function AppointmentForm(){
    const [technicians, setTechnicians] = useState([])
    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [technician, setTechnician] = useState('')
    const [reason, setReason] = useState('')

    const getData = async () =>{
        const response = await fetch('http://localhost:8080/api/technicians/')

        if(response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }
    const handleVinChange = (event) => setVin(event.target.value)

    const handleCustomerChange = (event) => setCustomer(event.target.value)

    const handleDateChange = (event) => setDate(event.target.value)

    const handleTimeChange = (event) => setTime(event.target.value)

    const handleTechnicianChange = (event) => setTechnician(event.target.value)

    const handleReasonChange = (event) => setReason(event.target.value)

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = `${date} ${time}`;
        data.technician_id = technician;
        data.reason = reason;
        
        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(appointmentUrl, fetchConfig);
        if(response.ok){
            setVin('')
            setCustomer('')
            setDate('')
            setTime('')
            setTechnician('')
            setReason('')
        }

    }
    useEffect (()=> {
        getData()
    }, [])

    return(
        <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="Vin" id="Vin" className="form-control" />
                            <label htmlFor="Vin">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="Customer" id="Customer" className="form-control" />
                            <label htmlFor="Customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateChange} value={date} placeholder="Date" required type="date" name="Date" id="Date" className="form-control" />

                            <label htmlFor="Date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTimeChange} value={time} placeholder="Time" required type="time" name="Time" id="Time" className="form-control" />
                            <label htmlFor="Time">Time in UTC</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleTechnicianChange} value={technician} required name="technician" id="technician" className="form-select">
                            <option value="">Choose a Technician</option>
                            {technicians.map(technician => {
                                return (
                                    <option key={technician.id} value={technician.id}>
                                        {technician.first_name} {technician.last_name}
                                    </option>
                                )
                                })}
                                </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="Reason" id="Reason" className="form-control" />
                            <label htmlFor="Reason">Reason</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}
export default AppointmentForm

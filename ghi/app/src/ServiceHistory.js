import {useEffect, useState} from 'react'

function ServiceHistory(){
    
    const [appointments, setAppointments] = useState([])
    const [search, setSearch] = useState('')

    const handleSearchChange = (event) => setSearch(event.target.value)

    const getData = async () =>{
        const response = await fetch('http://localhost:8080/api/appointments/')

        if(response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }

    useEffect (()=> {
        getData()
    }, [])

    return (
        <>
        <h1>Service History</h1>
        <div className="form-floating mb-3">
            <input onChange={handleSearchChange} value={search} placeholder="Search by Vin" required type="text" name="search" id="search" className="form-control" />
            <label htmlFor="search">Search by VIN</label>
        </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Vin</th>
                <th>Customer</th>
                <th>Date and Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            {appointments.filter((appointment)=>{
                return appointment.vin.includes(search)
            }).map(appointment => {
                return(
                <tr key={appointment.id}>
                <td>{ appointment.vin }</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date_time}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
                </tr>
                )
            })}
            </tbody>
        </table>
        </>
    )
}
export default ServiceHistory


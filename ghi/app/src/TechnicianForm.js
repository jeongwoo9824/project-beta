import { useState } from 'react';

function TechnicianForm(){
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [employeeID, setEmployeeID] = useState('')

    const handleSubmit = async (event)=>{
        event.preventDefault();

        const data = {}
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID;

        const technicianUrl = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(technicianUrl, fetchConfig);

        if(response.ok){
            setFirstName('')
            setLastName('')
            setEmployeeID('')
        }
    }

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value)
    }
    const handleLastNameChange = (event) => {
        setLastName(event.target.value)
    }
    const handleIDChange = (event) => {
        setEmployeeID(event.target.value)
    }

    return(
        <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} value={firstName} placeholder="First Name" required type="text" name="first name" id="first name" className="form-control" />
                            <label htmlFor="first name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} value={lastName} placeholder="Last Name" required type="text" name="last name" id="last name" className="form-control" />
                            <label htmlFor="last name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleIDChange} value={employeeID} placeholder="Employee ID" required type="text" name="employee id" id="employee id" className="form-control" />
                            <label htmlFor="employee id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}
export default TechnicianForm

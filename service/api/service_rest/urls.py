from django.urls import path
from .views import list_technicians, delete_technician, list_appointments, appointment_detail, appointment_canceled, appointment_finished


urlpatterns = [
    path("technicians/", list_technicians, name="list_technicians"),
    path("technicians/<int:pk>/", delete_technician, name="delete_technician"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:pk>/", appointment_detail, name="appointment_detail"),
    path('appointments/<int:pk>/cancel/', appointment_canceled, name="appointment_canceled"),
    path('appointments/<int:pk>/finish/', appointment_finished, name="appointment_finished"),
]
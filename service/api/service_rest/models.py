from django.db import models
from django.urls import reverse

class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("list_technicians", kwargs={"pk": self.pk})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=100, default="created")
    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        )

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("appointment_detail",kwargs={"pk": self.pk})
    
    def cancel_appointment(self):
        self.status= "canceled"
        self.save()

    def finish_appointment(self):
        self.status = "finished"
        self.save()


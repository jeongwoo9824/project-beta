from django.shortcuts import render
from .models import Technician, AutomobileVO, Appointment
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "sold",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )

    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Failed to create Technician"},
                status=400
            ) 


@require_http_methods(["DELETE"])
def delete_technician(request, pk):
    try:
        if request.method == "DELETE":
            count, _ = Technician.objects.filter(id=pk).delete()
            return JsonResponse(
                {"Deleted": count > 0}
            )
    except:
        return JsonResponse(
            {"message": "Failed to delete Technician"},
            status=400
        )            



@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(employee_id=technician_id)   
            content["technician"] = technician

            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Failed to create appointment"},
                status=400
            ) 


@require_http_methods(["DELETE"])
def appointment_detail(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Appointment.objects.filter(id=pk).delete()
            return JsonResponse(
                {"Deleted": count > 0}
            )
        except:
            return JsonResponse(
                {"message": "Failed to delete appointment"},
                status=400
            ) 


@require_http_methods(["PUT"])
def appointment_canceled(request, pk):

    appointment = Appointment.objects.get(id=pk)
    appointment.cancel_appointment()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def appointment_finished(request, pk):

    appointment = Appointment.objects.get(id=pk)
    appointment.finish_appointment()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )



